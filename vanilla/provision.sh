#!/usr/bin/env bash

apt-get update

# install python
apt-get install install python-setuptools python-dev build-essential -q -y

# install scipy stack
apt-get install python-numpy python-scipy python-matplotlib ipython ipython-notebook python-pandas python-sympy python-nose -q -y 

# install wxPython
apt-get install python-wxgtk2.8 -q -y

# install project requirments
# pip install -r requirements.txt