jenkins:
  lookup:
    home: /opt/jenkins
    plugins:
      scm-api:
        url: 'http://updates.jenkins-ci.org/latest/maven-plugin.hpi'
      installed:
        - bitbucket