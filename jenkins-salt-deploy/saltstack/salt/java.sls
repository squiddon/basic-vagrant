install_java:
  pkg.installed:
    - pkgs:
      - openjdk-7-jdk
      - openjdk-7-jre-headless